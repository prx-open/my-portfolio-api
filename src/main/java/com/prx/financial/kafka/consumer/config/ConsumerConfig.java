package com.prx.financial.kafka.consumer.config;

import com.prx.financial.kafka.consumer.model.InstrumentValueKafka;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class ConsumerConfig {

    @Value("${kafka.consumer.instrument.topic}")
    private String instrumentTopic;
    @Value("${kafka.server.url}")
    private String bootstrapAddress;

    @Bean
    public ConsumerFactory<String, InstrumentValueKafka> instrumentConsumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        props.put(org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG, instrumentTopic);
        props.put(org.apache.kafka.clients.consumer.ConsumerConfig.MAX_PARTITION_FETCH_BYTES_CONFIG, 20971520);
        props.put(org.apache.kafka.clients.consumer.ConsumerConfig.FETCH_MAX_BYTES_CONFIG, 20971520);
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), new JsonDeserializer<>(InstrumentValueKafka.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, InstrumentValueKafka> instrumentKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, InstrumentValueKafka> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(instrumentConsumerFactory());
        return factory;
    }

}

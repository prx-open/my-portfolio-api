// Version: 1.0
// Date: 2023-12-30
package com.prx.financial.cache.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * InstrumentValue.
 */
@Entity
public class InstrumentValueCache implements Serializable {

    @Id
    private UUID id;
    private String symbol;
    private BigDecimal bid;
    private BigDecimal ask;
    private BigDecimal price;
    private BigDecimal volume;
    private BigDecimal previousClose;
    private String instrumentType;
    private LocalDateTime dateTime;

    public InstrumentValueCache() {
        // Empty constructor
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public BigDecimal getBid() {
        return bid;
    }

    public void setBid(BigDecimal bid) {
        this.bid = bid;
    }

    public BigDecimal getAsk() {
        return ask;
    }

    public void setAsk(BigDecimal ask) {
        this.ask = ask;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public BigDecimal getPreviousClose() {
        return previousClose;
    }

    public void setPreviousClose(BigDecimal previousClose) {
        this.previousClose = previousClose;
    }

    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "InstrumentValue{" +
                "symbol='" + symbol + '\'' +
                ", bid=" + bid +
                ", ask=" + ask +
                ", price=" + price +
                ", volume=" + volume +
                ", previousClose=" + previousClose +
                ", instrumentType='" + instrumentType + '\'' +
                ", dateTime=" + dateTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InstrumentValueCache that)) return false;
        return Objects.equals(getSymbol(), that.getSymbol())
                && Objects.equals(getBid(), that.getBid())
                && Objects.equals(getAsk(), that.getAsk())
                && Objects.equals(getVolume(), that.getVolume());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSymbol(), getBid(), getAsk(), getVolume());
    }
}

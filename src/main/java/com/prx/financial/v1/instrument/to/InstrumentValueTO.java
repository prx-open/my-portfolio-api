package com.prx.financial.v1.instrument.to;

import com.prx.financial.model.InstrumentValue;

import java.math.BigDecimal;
import java.util.UUID;

public class InstrumentValueTO extends InstrumentValue {
    private UUID id;
    private BigDecimal open;
    private BigDecimal close;
    private BigDecimal high;
    private BigDecimal low;
    ;

    public InstrumentValueTO() {
        super();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getOpen() {
        return open;
    }

    public void setOpen(BigDecimal open) {
        this.open = open;
    }

    public BigDecimal getClose() {
        return close;
    }

    public void setClose(BigDecimal close) {
        this.close = close;
    }

    public BigDecimal getHigh() {
        return high;
    }

    public void setHigh(BigDecimal high) {
        this.high = high;
    }

    public BigDecimal getLow() {
        return low;
    }

    public void setLow(BigDecimal low) {
        this.low = low;
    }

    @Override
    public String toString() {
        return "InstrumentValueTO{" +
                "id=" + id +
                ", bid=" + getBid() +
                ", ask=" + getAsk() +
                ", price=" + getPrice() +
                ", open=" + open +
                ", close=" + close +
                ", high=" + high +
                ", low=" + low +
                ", volume=" + getVolume() +
                ", previousClose=" + getPreviousClose() +
                ", instrumentType='" + getInstrumentType() + '\'' +
                ", symbol='" + getSymbol() + '\'' +
                ", dateTime=" + getDateTime() +
                '}';
    }
}

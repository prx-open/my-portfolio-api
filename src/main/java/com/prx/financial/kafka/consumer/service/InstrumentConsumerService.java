package com.prx.financial.kafka.consumer.service;

import com.prx.financial.cache.model.InstrumentValueCache;
import org.apache.commons.lang.NotImplementedException;

import java.util.List;
import java.util.Optional;

/**
 * InstrumentService.
 */
public interface InstrumentConsumerService {

    default List<InstrumentValueCache> findAll() {
        throw new NotImplementedException();
    }

    default Optional<InstrumentValueCache> findById(String id) {
        throw new NotImplementedException();
    }

    default InstrumentValueCache save(InstrumentValueCache instrumentValueCache) {
        throw new NotImplementedException();
    }

    default List<InstrumentValueCache> saveAll(List<InstrumentValueCache> instrumentValueCaches) {
        throw new NotImplementedException();
    }

    default void deleteById(String id) {
        throw new NotImplementedException();
    }

    default void deleteAll() {
        throw new NotImplementedException();
    }

    default InstrumentValueCache update(InstrumentValueCache instrumentValueCache) {
        throw new NotImplementedException();
    }
}

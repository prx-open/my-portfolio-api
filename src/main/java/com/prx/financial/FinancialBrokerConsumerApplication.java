package com.prx.financial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableDiscoveryClient
@SpringBootApplication
@EnableMongoRepositories(basePackages = {"com.prx.financial.jpa.nosql.repository"})
public class FinancialBrokerConsumerApplication {

    public static void main(String[] args)  {
        SpringApplication.run(FinancialBrokerConsumerApplication.class, args);
    }
}

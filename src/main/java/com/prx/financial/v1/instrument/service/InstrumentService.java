package com.prx.financial.v1.instrument.service;

import com.prx.financial.v1.instrument.to.InstrumentValueTO;
import org.apache.commons.lang.NotImplementedException;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * InstrumentService.
 */
public interface InstrumentService {

    /**
     * Find all.
     *
     * @return the response entity
     */
    default ResponseEntity<List<InstrumentValueTO>> findAll() {
        throw new NotImplementedException();
    }

    /**
     * Find by id.
     *
     * @param instrumentType the instrument type
     * @return the response entity
     */
    default ResponseEntity<InstrumentValueTO> findById(String instrumentType) {
        throw new NotImplementedException();
    }
}

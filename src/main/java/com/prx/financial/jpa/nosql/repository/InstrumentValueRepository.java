package com.prx.financial.jpa.nosql.repository;

import com.prx.financial.jpa.nosql.domain.InstrumentValueEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InstrumentValueRepository extends MongoRepository<InstrumentValueEntity, String> {

}

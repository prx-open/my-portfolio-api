package com.prx.financial.kafka.consumer.service;

import com.prx.financial.cache.model.InstrumentValueCache;
import com.prx.financial.cache.repository.InstrumentValueCacheRepository;
import com.prx.financial.mapper.InstrumentValueCacheMapper;
import com.prx.financial.jpa.nosql.repository.InstrumentValueRepository;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@CacheConfig(cacheNames = "instruments")
public class InstrumentConsumerServiceImp implements InstrumentConsumerService {

    private final InstrumentValueCacheRepository instrumentValueCacheRepository;
    private final InstrumentValueRepository instrumentValueRepository;
    private final InstrumentValueCacheMapper instrumentValueCacheMapper;

    public InstrumentConsumerServiceImp(InstrumentValueCacheRepository instrumentValueCacheRepository, InstrumentValueRepository instrumentValueRepository, InstrumentValueCacheMapper instrumentValueCacheMapper) {
        this.instrumentValueCacheRepository = instrumentValueCacheRepository;
        this.instrumentValueRepository = instrumentValueRepository;
        this.instrumentValueCacheMapper = instrumentValueCacheMapper;
    }

    /** {@inheritDoc} */
    @Override
    public List<InstrumentValueCache> saveAll(List<InstrumentValueCache> instrumentValueCaches) {
        return instrumentValueCaches;
    }

    /** {@inheritDoc} */
    @Override
    @Cacheable(key = "#instrumentValueCache.id")
    public InstrumentValueCache save(InstrumentValueCache instrumentValueCache) {
        var instrumentValueEntity = instrumentValueCacheMapper.toSource(instrumentValueCache);
        instrumentValueRepository.save(instrumentValueEntity);
        return instrumentValueCacheRepository.save(instrumentValueCache);
    }
}

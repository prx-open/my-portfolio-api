package com.prx.financial.mapper;

import com.prx.financial.cache.model.InstrumentValueCache;
import com.prx.financial.v1.instrument.to.InstrumentValueTO;
import org.mapstruct.*;

/**
 * InstrumentValueToMapper.
 */
@Mapper(
        componentModel = "spring",
        uses = {InstrumentValueCache.class, InstrumentValueTO.class},
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
@MapperConfig(unmappedTargetPolicy = ReportingPolicy.ERROR, unmappedSourcePolicy = ReportingPolicy.ERROR)
public interface InstrumentValueToMapper {

    /**
     * To target.
     *
     * @param instrumentValueCache the instrument value to
     * @return the instrument value
     */
    InstrumentValueTO toTarget(InstrumentValueCache instrumentValueCache);

    /**
     * To source.
     *
     * @param instrumentValueTO the instrument value
     * @return the instrument value to
     */
    @InheritInverseConfiguration
    InstrumentValueCache toSource(InstrumentValueTO instrumentValueTO);
}

package com.prx.financial.kafka.consumer.model;

import java.util.Objects;

public class InstrumentValueKafka {
    private double bid;
    private double ask;
    private double price;
    private int volume;
    private double previous_close;
    private String quote_type;
    private String symbol;

    public InstrumentValueKafka() {
        // Empty constructor
    }

    public InstrumentValueKafka(double bid, double ask, double price, int volume, double previousClose, String quote_type, String symbol) {
        this.bid = bid;
        this.ask = ask;
        this.price = price;
        this.volume = volume;
        this.previous_close = previousClose;
        this.quote_type = quote_type;
        this.symbol = symbol;
    }

    public InstrumentValueKafka(double bid, double ask, int volume, double previousClose, String quote_type, String symbol) {
        this.bid = bid;
        this.ask = ask;
        this.volume = volume;
        this.previous_close = previousClose;
        this.quote_type = quote_type;
        this.symbol = symbol;
    }

    public InstrumentValueKafka(double price, int volume, double previousClose, String quote_type, String symbol) {
        this.bid = price;
        this.ask = price;
        this.price = price;
        this.volume = volume;
        this.previous_close = previousClose;
        this.quote_type = quote_type;
        this.symbol = symbol;
    }

    public double getBid() {
        return bid;
    }

    public void setBid(double bid) {
        this.bid = bid;
    }

    public double getAsk() {
        return ask;
    }

    public void setAsk(double ask) {
        this.ask = ask;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public double getPrevious_close() {
        return previous_close;
    }

    public void setPrevious_close(double previous_close) {
        this.previous_close = previous_close;
    }

    public String getQuote_type() {
        return quote_type;
    }

    public void setQuote_type(String quote_type) {
        this.quote_type = quote_type;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "InstrumentValue{" +
                "bid=" + bid +
                ", ask=" + ask +
                ", price=" + price +
                ", volume=" + volume +
                ", previous_close=" + previous_close +
                ", quote_type='" + quote_type + '\'' +
                ", symbol='" + symbol + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InstrumentValueKafka that)) return false;
        return Double.compare(that.getBid(), getBid()) == 0
                && Double.compare(that.getAsk(), getAsk()) == 0
                && Double.compare(that.getPrice(), getPrice()) == 0
                && getVolume() == that.getVolume()
                && Double.compare(that.getPrevious_close(), getPrevious_close()) == 0
                && Objects.equals(getQuote_type(), that.getQuote_type())
                && Objects.equals(getSymbol(), that.getSymbol());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBid(), getAsk(), getPrice(), getVolume(), getPrevious_close(), getQuote_type(), getSymbol());
    }

}

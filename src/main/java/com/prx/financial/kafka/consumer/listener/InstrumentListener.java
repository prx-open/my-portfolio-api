package com.prx.financial.kafka.consumer.listener;

import com.prx.financial.kafka.consumer.model.InstrumentValueKafka;
import com.prx.financial.mapper.InstrumentValueMapper;
import com.prx.financial.kafka.consumer.service.InstrumentConsumerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

@Service
public class InstrumentListener {
    private final Logger logger = LoggerFactory.getLogger(InstrumentListener.class);
    private final CountDownLatch latch = new CountDownLatch(3);
    private final CountDownLatch partitionLatch = new CountDownLatch(2);
    private final CountDownLatch filterLatch = new CountDownLatch(2);
    private final CountDownLatch instrumentLatch = new CountDownLatch(1);
    private final InstrumentConsumerService instrumentConsumerService;
    private final InstrumentValueMapper instrumentValueMapper;

    /**
     * MessageListener.
     */
    public InstrumentListener(InstrumentConsumerService instrumentConsumerService, InstrumentValueMapper instrumentValueMapper) {
        // Empty constructor
        this.instrumentConsumerService = instrumentConsumerService;
        this.instrumentValueMapper = instrumentValueMapper;
    }

    @KafkaListener(topics="${kafka.consumer.instrument.topic}", groupId = "instruments", containerFactory = "instrumentKafkaListenerContainerFactory")
    public void listenInstrument(InstrumentValueKafka message) {
        logger.info("Received Instrument Message in group 'instrument': {}", message);
        if(Objects.nonNull(message)) {
            var instrumentValueCache =  instrumentValueMapper.toTarget(message);
            instrumentValueCache.setId(UUID.randomUUID());
            instrumentValueCache.setDateTime(LocalDateTime.now());
            instrumentConsumerService.save(instrumentValueCache);
        }
        instrumentLatch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public CountDownLatch getPartitionLatch() {
        return partitionLatch;
    }

    public CountDownLatch getFilterLatch() {
        return filterLatch;
    }

    public CountDownLatch getInstrumentLatch() {
        return instrumentLatch;
    }
}

package com.prx.financial.v1.instrument.service;

import com.prx.financial.cache.repository.InstrumentValueCacheRepository;
import com.prx.financial.mapper.InstrumentValueToMapper;
import com.prx.financial.v1.instrument.to.InstrumentValueTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class InstrumentServiceImpl implements InstrumentService {

    private final InstrumentValueCacheRepository instrumentValueCacheRepository;
    private final InstrumentValueToMapper instrumentValueToMapper;

    public InstrumentServiceImpl(InstrumentValueCacheRepository instrumentValueCacheRepository, InstrumentValueToMapper instrumentValueToMapper) {
        this.instrumentValueCacheRepository = instrumentValueCacheRepository;
        this.instrumentValueToMapper = instrumentValueToMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<List<InstrumentValueTO>> findAll() {
        var result = instrumentValueCacheRepository.findAll();

        if (Collections.emptyList().equals(result)) {
            return ResponseEntity.ok(new ArrayList<>());
        } else {
            var instrumentValueToList = new ArrayList<InstrumentValueTO>();
            result.forEach(instrumentValueCache -> {
                var instrumentValueTO = instrumentValueToMapper.toTarget(instrumentValueCache);
                instrumentValueToList.add(instrumentValueTO);
            });
            return ResponseEntity.ok(instrumentValueToList);
        }
    }
}

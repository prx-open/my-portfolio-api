package com.prx.financial.v1.instrument.controller;

import com.prx.financial.v1.instrument.to.InstrumentValueTO;
import com.prx.financial.v1.instrument.service.InstrumentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/instrument")
public class InstrumentValueController {

    private final InstrumentService instrumentService;

    public InstrumentValueController(InstrumentService instrumentService) {
        this.instrumentService = instrumentService;
    }

    /**
     * Find all.
     *
     * @return the response entity
     */
    @GetMapping("/all")
    public ResponseEntity<List<InstrumentValueTO>> findAll() {
        return instrumentService.findAll();
    }
}

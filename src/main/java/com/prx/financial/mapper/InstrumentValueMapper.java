package com.prx.financial.mapper;

import com.prx.financial.cache.model.InstrumentValueCache;
import com.prx.financial.kafka.consumer.model.InstrumentValueKafka;
import org.mapstruct.*;

@Mapper(
        componentModel = "spring",
        uses = {InstrumentValueKafka.class, InstrumentValueCache.class},
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
@MapperConfig(unmappedTargetPolicy = ReportingPolicy.ERROR, unmappedSourcePolicy = ReportingPolicy.ERROR)
public interface InstrumentValueMapper {

        @Mapping(source="quote_type", target = "instrumentType")
        InstrumentValueCache toTarget(InstrumentValueKafka instrumentValueKafka);

        @InheritConfiguration
        InstrumentValueKafka toSource(InstrumentValueCache instrumentValueCache);
}

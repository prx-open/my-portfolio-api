package com.prx.financial.mapper;

import com.prx.financial.cache.model.InstrumentValueCache;
import com.prx.financial.jpa.nosql.domain.InstrumentValueEntity;
import org.mapstruct.*;

@Mapper(
        componentModel = "spring",
        uses = {InstrumentValueCache.class, InstrumentValueEntity.class},
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
@MapperConfig(unmappedTargetPolicy = ReportingPolicy.ERROR, unmappedSourcePolicy = ReportingPolicy.ERROR)
public interface InstrumentValueCacheMapper {

    InstrumentValueCache toTarget(InstrumentValueEntity instrumentValueEntity);

    @InheritInverseConfiguration
    InstrumentValueEntity toSource(InstrumentValueCache instrumentValueCache);
}

package com.prx.financial.cache.repository;

import com.prx.financial.cache.model.InstrumentValueCache;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InstrumentValueCacheRepository extends CrudRepository<InstrumentValueCache, String> {
}

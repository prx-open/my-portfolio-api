package com.prx.financial.jpa.nosql.domain;

import com.prx.financial.model.InstrumentValue;
import jakarta.persistence.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document(collection = "intraday_value")
public class InstrumentValueEntity extends InstrumentValue {
    @Id
    private UUID id;


    public InstrumentValueEntity() {
        super();
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public String toString() {
        return "InstrumentValueEntity{" +
                "id=" + id +
                ", bid=" + getBid() +
                ", ask=" + getAsk() +
                ", price=" + getPrice() +
                ", volume=" + getVolume() +
                ", previousClose=" + getPreviousClose() +
                ", instrumentType='" + getInstrumentType() + '\'' +
                ", symbol='" + getSymbol()+ '\'' +
                ", dateTime=" + getDateTime() +
                '}';
    }
}
